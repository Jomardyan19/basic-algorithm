﻿using System;

namespace ConsoleApp74
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("x = ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("y =  ");
            int y = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(Test(x,y));
        }
        public static int Test(int x, int y)
        {
            return x == y ? (x + y) * 3 : x + y;
        }
    }
}
